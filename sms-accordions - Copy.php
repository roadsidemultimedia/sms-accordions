<?php
/*
Plugin Name: SMS Accordion
Plugin URI: http://www.roadsidemultimedia.com
Description: Vertical Accordions. 
Author: Curtis Grant
PageLines: true
Version: 1.0.2
Section: true
Class Name: SMSAccordions
Filter: slider, component, misc
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-accordion
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class SMSAccordions extends PageLinesSection {

  function section_styles(){
    wp_enqueue_style( 'accordion-css', $this->base_url.'/css/accordions.css');
  }

  function section_opts(){


    $options = array();    
    
    $options[] =
      array(
        'type'  => 'multi',
        'col'   => 1,
        'title' => 'Accordion Options',
        'opts'  => array(
          array(
            'type'      => 'select',
            'key'     => 'TL_arrow_nav',
            'label'     => 'Arrow Nav (coming soon)',
            'col' => 1,
            'opts'      => array(
              'yes'   => array('name' => 'Yes'),
              'no'  => array('name' => 'No'),
                  )
              ),
          )
  );
$options[] =
      array(
        'key'   => 'array',
        'type'    => 'accordion', 
        'col'   => 2,
        'title' => 'Accordions',
        'opts_cnt'  => 4,
        'label'   => __( 'Accordions', 'pagelines' ),
        'opts'  => array(
          array(
              'key'   => 'smsaccordionhdr',
              'label'   => __( 'Header', 'pagelines' ),
              'type'    => 'text',
                ),
          array(
              'key'   => 'smsaccordiontext',
              'label'   => __( 'Text', 'pagelines' ),
              'type'    => 'textarea',
                ),
          )
      );

    
    return $options;

  }


  function get_content( $array ){
    
    $out = '';
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $smsacchdr = pl_array_get( 'smsaccordionhdr', $item );
        $smsacctext = pl_array_get( 'smsaccordiontext', $item ); 
        if( $tshdr ){
         
          $out .= sprintf(
            '<div class="smstoggle"><a href="#"><span class="toggle_icon"></span>%s</a></div><div style="display: none;" class="smstoggle-content">%s</div>',
            $smsacchdr,
            $smsacctext
          );
        }
      }
    }
    
    return $out;
  }

   function section_template( ) { 
    
    $array = $this->opt('array');
   ?>
  <div class="accordion">
            <?php

    $out = $this->get_content( $array );

    echo $out;

    ?>
    <div class="smstoggle">
      <a href="javascript:;"><span class="toggle_icon"></span>Accordion1</a>
    </div>
    <div style="display: none;" class="smstoggle-content">
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.Lorem 
    </div>
    <div class="smstoggle">
      <a href="javascript:;"><span class="toggle_icon"></span>Accordion 2</a>
    </div>
    <div style="display: none;" class="smstoggle-content">
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.Lorem 
    </div>
  </div>
  <script type="text/javascript">
  jQuery(document).ready(function($) {
    
    //*** Toggle ***//  

    $('.smstoggle-content').each(function() {
        if(!$(this).hasClass('default-open')){
            $(this).hide();
        }
    });

    $("div.smstoggle").click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass("active");
        }else{
            $(this).addClass("active");
        }
        return false;
    });

    $("div.smstoggle").click(function(){
        $(this).next(".smstoggle-content").slideToggle();
    });

});
  </script>

<?php 
}


}
<?php
/*
Plugin Name: SMS Accodions
Plugin URI: http://www.roadsidemultimedia.com
Description: Responsive Accordion for DMS SMS Version 1.1.2 or less
Author: Curtis Grant
PageLines: true
Version: 1.0.3
Section: true
Class Name: SMSAccordions
Filter: component
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-accordions
Bitbucket Branch: master
*/

if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class SMSAccordions extends PageLinesSection {

  function section_styles(){
        wp_enqueue_style( 'accordion-css', $this->base_url.'/css/accordions.css');
    }

  function section_opts(){


    $options = array();
    
    $options[] = array(
        'type'  => 'multi',
        'key' => 'config', 
        'title' => 'Config',
        'col' => 1,
        'opts'  => array(
          array(
            'key'     => 'smsacc_font_hdr',
            'type'      => 'select',
            'label'   => __( 'Header Font', 'pagelines' ),
            'opts'      => array(
              'font-serif'   => array('name' => 'Serif'),
              'font-serif-alt'  => array('name' => 'Serif (alt)'),
              'font-sans-serif'   => array('name' => 'Sans Serif'),            
              'font-condensed-serif'    => array('name' => 'Condensed Serif'),
              'font-condensed-sans-serif' => array('name' => 'Condensed Sans Serif'),
              'font-slab-serif'  => array('name' => 'Slab Serif'),
              'font-script'  => array('name' => 'Script'),
              'font-descriptive'  => array('name' => 'Decorative'),
            )
          ),
          array(
            'type'      => 'select',
            'key'     => 'smsacc_font_hdr_size',
            'label'     => 'Header Font Size',
            'opts'      => array(
              'fs-mini'   => array('name' => 'Mini'),
              'fs-small'  => array('name' => 'Small'),
              'fs-medium'   => array('name' => 'Medium'),            
              'fs-large'    => array('name' => 'Large'),
              'fs-xlarge' => array('name' => 'X-Large'),
              'fs-xxlarge'  => array('name' => 'XX-Large'),
              'fs-xxxlarge'  => array('name' => 'XXX-Large'),
              'fs-xxxxlarge'  => array('name' => 'XXXX-Large'),
            )
          ),
          array(
            'key'     => 'smsacc_color_hdr',
            'type'      => 'color',
            'label'   => __( 'Header Font color', 'pagelines' ),
          ),
          array(
            'key'     => 'smsacc_color_hdr_bg',
            'type'      => 'color',
            'label'   => __( 'Header BG color', 'pagelines' ),
          ),
          array(
            'key'     => 'smsacc_font_body',
            'type'      => 'select',
            'label'   => __( 'Text Font', 'pagelines' ),
            'opts'      => array(
              'font-serif'   => array('name' => 'Serif'),
              'font-serif-alt'  => array('name' => 'Serif (alt)'),
              'font-sans-serif'   => array('name' => 'Sans Serif'),            
              'font-condensed-serif'    => array('name' => 'Condensed Serif'),
              'font-condensed-sans-serif' => array('name' => 'Condensed Sans Serif'),
              'font-slab-serif'  => array('name' => 'Slab Serif'),
              'font-script'  => array('name' => 'Script'),
              'font-descriptive'  => array('name' => 'Decorative'),
            )
          ),
          array(
            'type'      => 'select',
            'key'     => 'smsacc_font_body_size',
            'label'     => 'Text Font Size',
            'opts'      => array(
              'fs-mini'   => array('name' => 'Mini'),
              'fs-small'  => array('name' => 'Small'),
              'fs-medium'   => array('name' => 'Medium'),            
              'fs-large'    => array('name' => 'Large'),
              'fs-xlarge' => array('name' => 'X-Large'),
              'fs-xxlarge'  => array('name' => 'XX-Large'),
              'fs-xxxlarge'  => array('name' => 'XXX-Large'),
              'fs-xxxxlarge'  => array('name' => 'XXXX-Large'),
            )
          ),
          array(
            'key'     => 'smsacc_color_body',
            'type'      => 'color',
            'label'   => __( 'Text Font color', 'pagelines' ),
          ),
          array(
            'key'     => 'smsacc_color_body_bg',
            'type'      => 'color',
            'label'   => __( 'Text BG color', 'pagelines' ),
          ),
        )
        
      );
    
    
    $options[] = array(
      'key'   => 'array',
        'type'    => 'accordion', 
      'col'   => 2,
      'opts_cnt'  => 4,
      'title'   => __('Accordions', 'pagelines'), 
      'opts'  => array(
        array(
          'key' => 'smsacc_hdr',
          'label' => __( 'Accordion Header', 'pagelines' ),
          'type'  => 'text',
        ),
        array(
          'key' => 'smsacc_text',
          'label' => __( 'Accordion Text', 'pagelines' ),
          'type'  => 'textarea',
        ),

      )
      );
  
    
    return $options;

  }


  function get_content( $array ){
    
    $hdrfont = ( $this->opt('smsacc_font_hdr') ) ? $this->opt('smsacc_font_hdr') : "";
    $hdrfontsize = ( $this->opt('smsacc_font_hdr_size') ) ? $this->opt('smsacc_font_hdr_size') : "";
    $hdrfontcolor = ( $this->opt('smsacc_color_hdr') ) ? $this->opt('smsacc_color_hdr') : "444444";
    $hdrbgcolor = ( $this->opt('smsacc_color_hdr_bg') ) ? $this->opt('smsacc_color_hdr_bg') : "cccccc";
    $textfont = ( $this->opt('smsacc_font_body') ) ? $this->opt('smsacc_font_body') : "";
    $textfontsize = ( $this->opt('smsacc_font_body_size') ) ? $this->opt('smsacc_font_body_size') : "";
    $textfontcolor = ( $this->opt('smsacc_color_body') ) ? $this->opt('smsacc_color_body') : "444444";
    $textbgcolor = ( $this->opt('smsacc_color_body_bg') ) ? $this->opt('smsacc_color_body_bg') : "ffffff";
    
    $out = '';
    $num = 1;
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $smsacchdr = pl_array_get( 'smsacc_hdr', $item ); 
        $smsacctext = pl_array_get( 'smsacc_text', $item ); 
        $smsacctext = wpautop($smsacctext);
        if( $smsacchdr ){
        
          $out .= sprintf(
            '<div class="accordion-group"><div class="accordion-heading %s %s" style="background-color:#%s;"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse%s" style="color:#%s;"><span class="icon icon-angle-left toggle_icon"></span>%s</a></div><div id="collapse%s" class="accordion-body collapse %s %s" style="height: 0px; background-color:#%s;"><div class="accordion-inner" style="color:#%s;">%s</div></div></div>',
            $hdrfont,
            $hdrfontsize,
            $hdrbgcolor,
            $num,
            $hdrfontcolor,
            $smsacchdr,
            $num,
            $textfont,
            $textfontsize,
            $textbgcolor,
            $textfontcolor,
            $smsacctext
          );
        }
        $num++;
      }
    }
    
    
    return $out;
  }

   function section_template( ) { 
    
    $array = $this->opt('array');
  
  ?>
  
  <div class="accordion" id="accordion2">
  
    <?php

    $out = $this->get_content( $array ); 

    echo $out;

    ?>
  </div>

      

<?php }


}